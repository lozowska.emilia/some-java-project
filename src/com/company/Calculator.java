package com.company;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner calculator = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Wybierz działanie: " +
                    " 1. Dodawanie" +
                    " 2. Odejmowanie" +
                    " 3. Mnożenie" +
                    " 4. Dzielnie");
            int method = calculator.nextInt();
            switch (method) {
                case 1:
                    System.out.println("Wybrano dodawanie");
                    break;
                case 2:
                    System.out.println("Wybrano odejmowanie");
                    break;
                case 3:
                    System.out.println("Wybrano mnożenie");
                    break;
                case 4:
                    System.out.println("Wybrano dzielenie");
                    break;
            }
            System.out.println("Podaj pierwszą liczbę:");
            int a = calculator.nextInt();
            System.out.println("Podaj drugą liczbę:");
            int b = calculator.nextInt();
            if (method == 1) {
                int dodawanie = a + b;
                System.out.println("Wynik dodawania to: " + dodawanie);
            } else if (method == 2) {
                int odejmowanie = a - b;
                System.out.println("Wynik odejmowania to: " + odejmowanie);
            } else if (method == 3) {
                int mnozenie = a * b;
                System.out.println("Wynik mnożenia to: " + mnozenie);
            } else {
                int dzielenie = a / b;
                System.out.println("Wynik dzielenia to: " + dzielenie);
            }
        }

    }
    }
